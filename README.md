# onnx graph tools

Utilities for onnx files

## extract.py
Simple script that takes an onnx serialized model and lists the initializers.
Example use:
```bash
./extract.py --help
./extract.py --file=sparsezoo/yolov5s.onnx --list  --output-format="{name} {stype}:{dims} {sparsity:.3}%"
./extract.py --file=sparsezoo/yolov5s.onnx --list  --output-format="{name}: {tensor}" --node="model.9.m.0.cv2.conv.weight"
```

Can also be used to generate svg
Example use:
```bash
./extract.py --file=sparsezoo/yolov5s.onnx --svg=yolo.svg
google-chrome yolo.svg
```

Vanilla code of convolution can be dumped using `--code option` (option `-l` required):
Example use:
```bash
./extract.py --file=sparsezoo/yolov5s.onnx -l -n Conv_227 -c
```
