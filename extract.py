#!/usr/bin/env python3
from email.policy import default
import graphlib
import onnx
from optparse import OptionParser
import ipdb
import numpy as np
from sys import stderr
import json
from collections import defaultdict
from typing import Any, Callable, Dict, Optional
import pydot  # type: ignore
import os
from string import Template


OP_STYLE = {
    "shape": "box",
    "color": "#0F9D58",
    "style": "filled",
    "fontcolor": "#FFFFFF",
}

BLOB_STYLE = {"shape": "octagon"}

_NodeProducer = Callable[[onnx.NodeProto, int], pydot.Node]

def onnx_attr_stype(dtype):
    match dtype:
        case onnx.AttributeProto.FLOAT:
            return 'float'
        case onnx.AttributeProto.INT:
            return 'int'
        case onnx.AttributeProto.STRING:
            return 'string'
        case onnx.AttributeProto.TENSOR:
            return 'tensor'
        case onnx.AttributeProto.SPARSE_TENSOR:
            return 'sparse tensor'
        case onnx.AttributeProto.GRAPH:
            return 'graph'
        case onnx.AttributeProto.TYPE_PROTO:
            return 'type proto'
        case onnx.AttributeProto.FLOATS:
            return 'floats'
        case onnx.AttributeProto.INTS:
            return 'ints'
        case onnx.AttributeProto.STRINGS:
            return 'strings'
        case onnx.AttributeProto.TENSORS:
            return 'tensors'
        case onnx.AttributeProto.SPARSE_TENSORS:
            return 'sparse tensors'
        case onnx.AttributeProto.GRAPHS:
            return 'graphs'
        case onnx.AttributeProto.TYPE_PROTOS:
            return 'type protos'
        case _:
            raise ValueError(f"Unsupported ONNX attribute type: {dtype}")
    

def onnx_tensor_stype(dtype):
    match dtype:
        case onnx.TensorProto.DataType.FLOAT: 
            return 'float'
        case onnx.TensorProto.DataType.DOUBLE: 
            return 'double'
        case onnx.TensorProto.DataType.INT32: 
            return 'int32'
        case onnx.TensorProto.DataType.INT64: 
            return 'int64'
        case onnx.TensorProto.DataType.UINT64: 
            return 'uint64'
        case _: 
            stderr.write(f"Unsupported type {dtype}")
            raise NotImplementedError


def nptensor(tensor):
    dtype=tensor.data_type
    stype=onnx_tensor_stype(dtype)
    match stype:
        case 'float': 
            nptensor=np.frombuffer(tensor.raw_data,dtype=np.float32).reshape(tensor.dims)
        case 'double': 
            nptensor=np.frombuffer(tensor.raw_data,dtype=np.double).reshape(tensor.dims)
        case 'int32': 
            nptensor=np.frombuffer(tensor.raw_data,dtype=np.int32).reshape(tensor.dims)
        case 'int64': 
            nptensor=np.frombuffer(tensor.raw_data,dtype=np.int64).reshape(tensor.dims)
        case 'uint64': 
            nptensor=np.frombuffer(tensor.raw_data,dtype=np.uint64).reshape(tensor.dims)
        case _: 
            stderr.write(f"Unsupported type {dtype}")
            raise NotImplementedError
    return nptensor    

class richgraph:
    static_tensor_list=list()
    static_tensor_dict=dict()
    operator_list=list()
    operator_dict=dict()
    inferred_tensor_list=list()
    inferred_tensor_dict=dict()

    def __init__(self, model):
        inferred_model=onnx.shape_inference.infer_shapes(model)
        self.onnx_graph=inferred_model.graph
        for node in self.onnx_graph.initializer:
            static_tensor(node, self)
        for node in self.onnx_graph.value_info:
            inferred_tensor(node, self)
        for node in self.onnx_graph.node:
            operator(node, self)
 
class operator:
    def __init__(self, node, rgraph: richgraph):
        self.onnx_node=node
        self.rgraph=rgraph
        self.name=node.name
        self.op_type=node.op_type
        self.input_names=node.input
        self.output_names=node.output
        self.attributes=node.attribute # list
        # TODO node.attribute[0].name / .type / tensor => .t| INT => .i | INTS => ints...
        rgraph.operator_list.append(self)
        rgraph.operator_dict[self.name]=self

class inferred_tensor:
    def __init__(self, node, rgraph: richgraph):
        self.onnx_node=node
        self.rgraph=rgraph
        self.name=node.name
        self.stype=onnx_tensor_stype(node.type.tensor_type.elem_type)
        self.dims=list()
        for dim in node.type.tensor_type.shape.dim:
            if dim.dim_value != 0:
                self.dims.append(dim.dim_value)
        rgraph.inferred_tensor_list.append(self)
        rgraph.inferred_tensor_dict[self.name]=self

class static_tensor:
    def __init__(self, node, graph: richgraph):
        self.onnx=node
        self.graph=graph
        self.name=node.name
        self.dims=node.dims
        self.size=np.prod(node.dims)
        self.raw=node.raw_data
        self.length=len(node.raw_data)
        self.stype=onnx_tensor_stype(node.data_type)
        self.tensor=nptensor(node)
        self.sparsity=np.count_nonzero(self.tensor)*100/self.size
        graph.static_tensor_list.append(self)
        graph.static_tensor_dict[self.name]=self
 
class attr_tensor:
    def __init__(self, attr: onnx.AttributeProto):
        assert attr.type == onnx.AttributeProto.TENSOR
        self.name = attr.name
        self.dims = attr.t.dims
        self.size=np.prod(attr.t.dims)
        self.stype=onnx_tensor_stype(attr.t.data_type)
        self.tensor = nptensor(attr.t)
        self.raw=attr.t.raw_data
        self.length=len(self.raw)


def _escape_label(name: str) -> str:
    # json.dumps is poor man's escaping
    return json.dumps(name)


def _form_and_sanitize_docstring(s: str) -> str:
    url = "javascript:alert("
    url += _escape_label(s).replace('"', "'").replace("<", "").replace(">", "")
    url += ")"
    return url


def GetOpNodeProducer(embed_docstring: bool = False, use_short_name: bool = True, **kwargs: Any) -> _NodeProducer:
    def ReallyGetOpNode(op: onnx.NodeProto, op_id: int) -> pydot.Node:
        if use_short_name:
            node_name = f'{op.op_type}'
        else:
            if op.name: node_name = f'{op.name}/{op.op_type} (op#{op_id})'
            else: node_name = f'{op.op_type} (op#{op_id})'
        if len(op.input_names)>0: node_name += '\n Inputs&#58; ' + ' '.join(op.input_names) 
        if len(op.output_names)>0: node_name += '\n Outputs&#58; ' + ' '.join(op.output_names) 
        node = pydot.Node(node_name, **kwargs)
        if embed_docstring:
            url = _form_and_sanitize_docstring(op.onnx_node.doc_string)
            node.set_URL(url)
        return node

    return ReallyGetOpNode


def GetPydotGraph(
    rgraph: richgraph,
    name: Optional[str] = None,
    rankdir: str = "LR",
    node_producer: Optional[_NodeProducer] = None,
    embed_docstring: bool = False,
) -> pydot.Dot:
    if node_producer is None:
        node_producer = GetOpNodeProducer(embed_docstring=True, use_short_name=True, **OP_STYLE)
    pydot_graph = pydot.Dot(name, rankdir=rankdir)
    pydot_nodes: Dict[str, pydot.Node] = {}
    pydot_node_counts: Dict[str, int] = defaultdict(int)    
    pretty_node={tensor.name: pretty_inferred_tensor(tensor, '{name}\\n {stype}:{dims}') for tensor in rgraph.inferred_tensor_list}
    for tensor in rgraph.static_tensor_list:
        name = tensor.name
        label = pretty_static_tensor(tensor, '{name}\\n{stype} &#58;{dims}\\n{sparsity:.0f}%')
        node = pydot.Node(
            _escape_label(name + str(pydot_node_counts[name])),
            label=_escape_label(label),
            **BLOB_STYLE,
        )
        pydot_nodes[name] = node
        pydot_graph.add_node(node)
    for op_id, op in enumerate(rgraph.operator_list):
        op_node = node_producer(op, op_id)
        pydot_graph.add_node(op_node)
        for input_name in op.input_names:
            if input_name not in pydot_nodes:
                if input_name in pretty_node:
                    info=pretty_node[input_name]
                else: info=input_name
                input_node = pydot.Node(
                    _escape_label(input_name + str(pydot_node_counts[input_name])),
                    label=_escape_label(info),
                    **BLOB_STYLE,
                )
                pydot_nodes[input_name] = input_node
                pydot_graph.add_node(input_node)
            else:
                input_node = pydot_nodes[input_name]
            pydot_graph.add_edge(pydot.Edge(input_node, op_node))
        for output_name in op.output_names:
            if output_name in pydot_nodes:
                pydot_node_counts[output_name] += 1
            if output_name in pretty_node:
                info=pretty_node[output_name]
            else: info=output_name
            output_node = pydot.Node(
                _escape_label(output_name + str(pydot_node_counts[output_name])),
                label=_escape_label(info),
                **BLOB_STYLE,
            )
            pydot_nodes[output_name] = output_node
            pydot_graph.add_node(output_node)
            pydot_graph.add_edge(pydot.Edge(op_node, output_node))
    return pydot_graph


def list_nodes(rgraph: richgraph, code: bool, format:str, node_name:str):
    assert isinstance(rgraph, richgraph)

    print("Initializers:\n")
    for tensor in rgraph.static_tensor_list: 
        if (node_name is None  or tensor.name == node_name):
            print(pretty_static_tensor(tensor, format))

    print("Operators:")
    for op in rgraph.operator_list:
        if node_name is None or op.name == node_name:
            if code is True:
                if op.op_type == 'Conv':
                    print(Conv_to_c(op))
                else:
                    NotImplementedError
            else:
                print('\n'.join(pretty_operator(op, rgraph)))

def pretty_inferred_tensor(tensor: inferred_tensor, format: str):
    assert isinstance(tensor,inferred_tensor)

    name=tensor.name
    dims=tensor.dims
    stype=tensor.stype
    sparsity=None
    size=None
    length=None
    tensor=None
    return eval(f"f'{format}'")

def pretty_attribute(attr: onnx.AttributeProto, format: str):
    name = attr.name
    dtype = attr.type
    stype = onnx_attr_stype(dtype)
    if dtype == onnx.AttributeProto.TENSOR:
        tensor = attr_tensor(attr)
        value = pretty_static_tensor(tensor, '{stype}{dims}: {tensor}')
    else:
        value = onnx.helper.get_attribute_value(attr)
    return eval(f"f'{format}'")


def pretty_operator(op: operator, rgraph: richgraph, 
                    op_format='{name}: {type}', 
                    attr_format='{name}({stype}): {value}',
                    static_tensor_format='{stype}{dims} {sparsity:.0f}%',
                    inferred_tensor_format='{stype}{dims}'):
    assert isinstance(op, operator) and isinstance(rgraph, richgraph)

    name=op.name
    type=op.op_type
    res=[eval(f"f'{op_format}'")]
    for attr in op.attributes:
        if attr_format is not None:
            res.append('\t' + pretty_attribute(attr, attr_format))
    inputs_outputs = [('input', input_name) for input_name in op.input_names] + \
        [('output', output_name) for output_name in op.output_names]
    for position, name in inputs_outputs:
        if name in rgraph.static_tensor_dict: 
            tensor=rgraph.static_tensor_dict[name]
            if static_tensor_format is not None:
                res.append(f'\t{position} tensor: ' + pretty_static_tensor(tensor, static_tensor_format))
        elif name in rgraph.inferred_tensor_dict:
            tensor=rgraph.inferred_tensor_dict[name]
            if inferred_tensor_format is not None:
                res.append(f'\t{position} tensor: ' + pretty_inferred_tensor(tensor, inferred_tensor_format))
    return res       

def pretty_static_tensor(tensor, format: str):
    assert isinstance(tensor, static_tensor) or isinstance(tensor, attr_tensor)
    if isinstance(tensor, static_tensor):
        name=tensor.name
        sparsity=tensor.sparsity
    length=tensor.length
    raw=tensor.raw
    dims=tensor.dims
    size=tensor.size
    stype=tensor.stype
    tensor=tensor.tensor
    return eval(f"f'{format}'")


def Conv_to_c(op: operator):
    assert isinstance(op, operator)
    # Here I assume no padding. 
    rgraph=op.rgraph
    weights_tensor_name=op.input_names[1]
    weights_tensor=rgraph.static_tensor_dict[weights_tensor_name]
    weights_string=np.array2string(weights_tensor.tensor, separator=',', max_line_width=160, threshold=np.inf).replace('[','{').replace(']','}')
    weight_dims=weights_tensor.dims

    bias_tensor_name=op.input_names[2]
    bias_tensor=rgraph.static_tensor_dict[bias_tensor_name]
    bias_dims=bias_tensor.dims

    input_tensor_name=op.input_names[0] 
    # /!\ won't work for actual input of network that is not inferred. TODO: add input node in rgraph
    input_tensor=rgraph.inferred_tensor_dict[input_tensor_name]
    input_dims=input_tensor.dims

    output_tensor_name=op.output_names[0] 
    output_tensor=rgraph.inferred_tensor_dict[output_tensor_name]
    output_dims=output_tensor.dims
 
    assert input_dims[0]==weight_dims[1]; #c
    assert weight_dims[0]==bias_dims[0] and weight_dims[0]==output_dims[0] #f
    assert input_dims[1]==output_dims[1] #x
    assert input_dims[2]==output_dims[2] #y
    values=dict(C=input_dims[0],
            F=weight_dims[0],
            X=input_dims[1],
            Y=input_dims[2],
            W=weight_dims[2],
            H=weight_dims[3],
            kernel_type=weights_tensor.stype,
            kernel=weights_string)

    #print(f'{x=}, {y=}, {c=}, {f=}, {w=}, {h=}\n')
    with open('vanilla_loop.tp', 'r') as f:
        template = Template(f.read())
        res=template.substitute(values)
    return res

def main():
    usage = "usage: %prog [options] ONNX_FILENAME"
    parser = OptionParser(usage=usage)   
    #parser.add_option("-f", "--filename", type=str, help="onnx input file", default="yolov5s.onnx")
    parser.add_option("-n", "--node", type=str, dest="nodename", help="grep node name", default=None)
    parser.add_option("-c", "--code", dest="code", action="store_true", default=False, help="generate C code")
    parser.add_option("-l", "--list", dest="list", action="store_true", default=False, help="List tensors")
    parser.add_option("-o", "--output-format", type=str, dest="output_format", help="Output format for tensors. Possible variables are {name}, {tensor}, {dims}, {stype}, {size}, {sparsity}", default="{name}")
    parser.add_option("-g", "--svg", type=str, dest="svgfile", default=False, help="Export graph as svg file")

    (options,args)=parser.parse_args()
    if len(args)==1:
        onnx_filename = args[0]
    else:
        parser.error("missing ONNX_FILENAME")

    model = onnx.load(onnx_filename);
    onnx.checker.check_model(model);
    rgraph=richgraph(model)

    if options.list:
        list_nodes(rgraph=rgraph, 
            code=options.code, 
            format=options.output_format, 
            node_name=options.nodename)
    if options.svgfile:
        pydot_graph = GetPydotGraph(rgraph, name=model.graph.name, rankdir="LR",
            node_producer=GetOpNodeProducer(embed_docstring=True, use_short_name=True, **OP_STYLE),)
        pydot_graph.write_dot("/tmp/onnx_extract.dot")  
        os.system(f"dot -Tsvg /tmp/onnx_extract.dot -o {options.svgfile}")

if __name__ == "__main__":
    main()


